# Ansible Physics Festival
![](files/scooby-and-gang.jpg)
Repo for the remote deployment of the infrastructure for the Texas A&M Physics festival.
## Deployment instructions
1. Download this repository to a control computer
2. Set up ssh to mystery machine.
3. Type the following from the repository and hope for the best:


`ansible-playbook meddling-kids.yml`

## Groups
There two groups of computers

### mystery-inc
These are the remote computers for filming:
- scooby
- shaggy
- velma
- daphne
- fred

### mystery-machine
This is the control node and the point where video is composited to be streamed to the internet.

## Roles
Each group has roles.

### gumshoe
Gumshoes are detectives.  They inspect clues, collect information, and send it to the group to share.  The members of mystery-inc are all gumshoes.

### broadcaster
A broadcaster collects information, sorts it, and distributes it to the larger world. mystery-machine is the broadcaster.
