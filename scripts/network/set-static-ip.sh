#!/bin/bash

nmcli con mod     "Wired connection 1" \
  ipv4.addresses  "128.194.160.14/23" \
  ipv4.gateway    "128.194.160.1" \
  ipv4.dns        "128.194.254.1, 128.194.254.2, 128.194.254.3" \
  ipv4.dns-search "physics.tamu.edu, tamu.edu" \
  ipv4.method     "manual" \
