#!/bin/bash

autossh -M 0 -f -N -o "ServerAliveInterval 30" -o "ServerAliveCountMax 3" -R $1:localhost:22 $2
