#!/bin/bash

#GST_DEBUG=3 gst-launch-1.0 -v v4l2src device=/dev/"$1" \
#             ! image/jpeg ! queue ! jpegdec ! videoconvert ! autovideosink


gst-launch-1.0 -v v4l2src device=/dev/"$1"\
             ! video/x-raw,framerate=30/1 \
			  ! videoconvert ! autovideosink
