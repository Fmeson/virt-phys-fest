#!/bin/bash

gst-launch-1.0 v4l2src device=/dev/video0 \
             ! video/x-raw,framerate=30/1 \
			 ! queue ! videoconvert \
             ! x264enc tune=zerolatency \
					   bitrate=5000 \
					   speed-preset=superfast \
             ! rtph264pay \
             ! udpsink host=128.194.161.71\
                       port=5000
