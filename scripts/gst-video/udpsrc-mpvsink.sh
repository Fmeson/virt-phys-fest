#!/bin/bash

gst-launch-1.0 udpsrc port=5000 ! \
               application/x-rtp,\
               encoding-name=JPEG,payload=30 ! \
               rtpjpegdepay ! jpegdec ! autovideosink
