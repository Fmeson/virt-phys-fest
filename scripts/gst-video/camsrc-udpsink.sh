#!/bin/bash

GST_DEBUG=3 gst-launch-1.0 v4l2src device=/dev/"$1"\
             ! video/x-raw,framerate=30/1,format=YUY2 \
             ! queue ! jpegenc ! rtpjpegpay\
             ! udpsink host=128.194.161.71\
                       port=5000"$2"

