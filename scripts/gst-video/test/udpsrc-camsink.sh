#!/bin/bash

gst-launch-1.0 udpsrc port=500"$1$2" \
             ! application/x-rtp,encoding-name=JPEG,payload=26 \
             ! rtpjpegdepay ! jpegdec \
			 ! queue ! autovideoconvert ! v4l2sink device=/dev/video"$1$2"

#gst-launch-1.0 udpsrc port=5000 \
#             ! application/x-rtp,encoding-name=JPEG,payload=26 \
#             ! rtpjpegdepay ! jpegdec ! queue ! videoconvert \
#			 ! videorate ! video/x-raw,framerate=30/1 \
#             ! v4l2sink device=/dev/video10

#gst-launch-1.0 udpsrc port=5000 \
#             ! application/x-rtp,encoding-name=JPEG,payload=26 \
#             ! rtpjpegdepay ! jpegdec \
#             ! v4l2sink device=/dev/video10
