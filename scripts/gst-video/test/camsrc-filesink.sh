#!/bin/bash

gst-launch-1.0 v4l2src device=/dev/video0 num-buffers=500
                     ! video/x-raw, framerate=30/
                     ! jpegenc ! avimux \
                     ! filesink location=mjpeg.avi
