#!/bin/bash

gst-launch-1.0 udpsrc port=50000 ! \
               application/x-rtp,\
               encoding-name=JPEG,payload=30 ! \
               rtpjpegdepay ! jpegdec ! autovideosink
