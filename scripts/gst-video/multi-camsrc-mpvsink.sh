#!/bin/bash

#GST_DEBUG=3 gst-launch-1.0 -v v4l2src device=/dev/"$1" \
#             ! image/jpeg ! queue ! jpegdec ! videoconvert ! autovideosink

for i in {0..10}; do
sleep 1
gst-launch-1.0 -v v4l2src device=/dev/video"$i"\
             ! video/x-raw,framerate=30/1 \
			  ! videoconvert ! autovideosink &
done
